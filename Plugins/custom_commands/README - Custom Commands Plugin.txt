Custom Commands Plugin
Version 1.11
Daniel Paul (danpaul88)
danpaul88@yahoo.co.uk


The custom commands plugin allows you to quickly and easily setup your own
!commands in brenbot without the need to create a specific plugin for them.

Details of how commands should be setup are inside the custom_commands.brf
file.

If you want to ask any questions, request new features or report bugs please
post on the Renegade Public forums (www.renegadeforums.com) in the Technical
Support -> Other area, or email me.


###############
Installation
###############

Extract the plugins folder and the custom_commands.brf file to your BRenBot
folder.


###############
Changelog
###############

1.11
 - Support for updating through brLoader
 
1.10
 - Added support for the %args tag in commands

1.0
 - First Release


###############
Commands
###############

!show_commands
Lists all the available commands.