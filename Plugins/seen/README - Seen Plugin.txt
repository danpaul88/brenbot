Seen Plugin
Version 1.04
Daniel Paul (danpaul88)
danpaul88@yahoo.co.uk


The seen plugin remembers when players were last in the game, and can tell you how long it has been since then.

This plugin re-creates the function of the !seen command as it existed in brenbot 1.41.

If you want to ask any questions, request new features or report bugs please post on the Renegade Public forums (www.renegadeforums.com) in the Technical Support -> Other area, or email me.


###############
Installation
###############

Extract the plugins folder to your BRenBot folder.


###############
Changelog
###############

1.04
 - Support for updating through brLoader
 
1.03
 - Updated to be backwards compatible with BRenBot 1.50

1.02
 - Updated to support BRenBot 1.51. Will NOT work on earlier versions

1.01
 - Minor bugfix to allow you to use !seen on full names which are part of someone elses name.


1.0
 - First Release


###############
Commands
###############

!seen <name>
Reports how long it has been since the specified player was in the game.