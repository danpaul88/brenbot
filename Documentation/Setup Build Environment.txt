##################################################################################
#
# Setting up a BRenBot 1.x development and build environment
#
# Author: danpaul88
# Revision: 1.11
# Date: 17-04-2020
#
##################################################################################


1) Download and install Strawberry Perl 5.30.2 (64 bit) from

   http://strawberryperl.com/download/5.30.2.1/strawberry-perl-5.30.2.1-64bit.msi

   OR, if you want to run BRenBot on a 32 bit operating system, use the 32 bit version;

   http://strawberryperl.com/download/5.30.2.1/strawberry-perl-5.30.2.1-32bit.msi




2) After installing Strawberry Perl open a command prompt (WinKey+R, cmd, enter) and type the
   following command;

   cpan -i -T POE Class::Unload

   Once this command has completed you should be able to run brenbot.pl successfully from the command
   line using 'perl brenbot.pl'




3) (Optional) If you also wish to be able to compile the source into an exe file, run the following
   command;

   cpan -i pp

   Once this command has completed you should be able to compile brenbot into a distributable package
   using the instructions in Packaging Command Lines.txt, or by running compile_exe.bat

